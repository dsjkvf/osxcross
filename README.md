osxcross
========


## About

Contains files and instructions for compiling macOS binaries on Linux using [osxcross](https://github.com/tpoechtrager/osxcross)


## Installation

  1. Clone https://github.com/tpoechtrager/osxcross
  2. Ensure you have all the dependencies it requires:
    - Clang 3.4+
    - cmake
    - git
    - patch
    - Python
    - libssl-dev
    - liblzma-dev
    - libxml2-dev
    - bash shell
  3. To produce an SDK tarball
    3.1. either:
      a) Download Xcode: https://developer.apple.com/download/more/
      b) Package the SDK: https://github.com/tpoechtrager/osxcross#packaging-the-sdk
    3.2. or:
      a) `wget https://bitbucket.org/dsjkvf/osxcross/downloads/MacOSX10.11.sdk.tar.xz`
  5. Copy the resulted tarball to osxcross/tarballs
  6. Run `./build.sh`
  7. Move `osxcross/target/*` to the folder of your choice, add folder-of-your-choice/bin/ to the path.


## Building

  1. Sample Go build command:

    env LD_LIBRARY_PATH=/home/s/go-cross/lib OSXCROSS_NO_INCLUDE_PATH_WARNINGS=1 MACOSX_DEPLOYMENT_TARGET=10.8 CC=o64-clang CXX=o64-clang++ GOOS=darwin GOARCH=amd64 CGO_ENABLED=1 make

  2. Sample Rust build command:

    env LD_LIBRARY_PATH=/home/s/go-cross/lib OSXCROSS_NO_INCLUDE_PATH_WARNINGS=1 MACOSX_DEPLOYMENT_TARGET=10.8 CC=o64-clang CXX=o64-clang++ cargo build --release --target=x86_64-apple-darwin
